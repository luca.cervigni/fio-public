#!/bin/bash

#cleanup part
echo "Removing all existing servers..."
openstack server delete $(openstack server list -f value | grep -v IBM | awk '{print $1}') #remove all servers in the project except the IBM workaround servers
sleep 60
echo "Removing all existing volumes..."
openstack volume delete $(openstack volume list -f value | grep -v image- | grep -v IBM | awk '{print $1}')
