#!/bin/bash
#this script runs from the vm

BASE_DIR=/home/ubuntu/fio-public
source $BASE_DIR/config
source $BASE_DIR/run.config

#env setup

#install fio and jq if fio not present

if ! command -v fio &> /dev/null
then
    echo "Installing fio and jq ..."
    sudo apt-get update
    sudo apt-get install fio jq -y
fi
#create fio files directory
mkdir -p ${TEMP_DIR}
mkdir -p ${RESULTS_DIR}

TESTS='4K-read 4K-write 4M-read 4M-write'

#concurrent start based on STARTTIME
STARTTIMEFILE=$BASE_DIR/starttime
STARTTIME=$(cat $STARTTIMEFILE)

echo "Starting test at $STARTTIME ..."
current_epoch=$(date +%s)
target_epoch=$(date -d "$STARTTIME" +%s)
sleep_seconds=$(( $target_epoch - $current_epoch ))
sleep $sleep_seconds

for nrun in $(seq 01 $NRUNS)
do
  for test in $TESTS
  do
    sync
    sudo su -c 'echo 3 > /proc/sys/vm/drop_caches'
    echo "RUN: $nrun - Running $test"
    fio ${BASE_DIR}${SCRIPT_DIR}/tests.fio --section $test --runtime $RUNTIME --ramp_time $RAMPTIME --directory $TEMP_DIR --output=${RESULTS_DIR}/${test}.result-$nrun --output-format=json
  done
done

#synthetic results in a summary file split line between tests runs
for result in $(ls ${RESULTS_DIR} | grep -v $SUMMARY_FILE )
do
  TESTNAME=$(jq ".jobs | .[].jobname" ${RESULTS_DIR}/$result)
  if [[ "$result" == *"4K"* ]]; then
    if [[ "$result" == *"read"* ]]; then
      IOPSREAD=$(jq ".jobs | .[].read.iops" ${RESULTS_DIR}/$result)
      IOPSREAD=${IOPSREAD%.*}
      echo "Testname: $TESTNAME - IOPS: $IOPSREAD" >> ${SUMMARY_RESULTS}
    else
      IOPSWRITE=$(jq ".jobs | .[].write.iops" ${RESULTS_DIR}/$result)
      IOPSWRITE=${IOPSWRITE%.*}
      echo "Testname: $TESTNAME - IOPS: $IOPSWRITE" >> ${SUMMARY_RESULTS}
    fi
  fi
  if [[ "$result" == *"4M"* ]]; then
    if [[ "$result" == *"read"* ]] ; then
      BWREAD=$(jq ".jobs | .[].read.bw" ${RESULTS_DIR}/$result)
      echo "Testname: $TESTNAME - BW: $((BWREAD / 1024)) MBps" >> ${SUMMARY_RESULTS}
    else
      BWWRITE=$(jq ".jobs | .[].write.bw" ${RESULTS_DIR}/$result)
      echo "Testname: $TESTNAME - BW: $((BWWRITE / 1024)) MBps" >> ${SUMMARY_RESULTS}
    fi
  fi
done
#file split
sed '0~'$NRUNS' a\\' ${SUMMARY_RESULTS} | awk '{print $5}' > ${SUMMARY_RESULTS}_split
