#!/bin/bash
#Use to commit the new values to the benchmark, and to create vms

echo "WARNING! - This script is destructive, it will delete all Volumes and all servers within the project... press y to continue"

read stdin

if [[ $stdin != [Yy] ]]; then
    echo "Interrupted by user."
    exit
fi

rm $LOGFILE

#env preparation
BASE_DIR=$(pwd)
source config
source $RUN_CONFIG

OPENRC=$1
source $OPENRC

#configure coordinated start time
date -d "$SYNCSTARTAFTER" +'%H:%M' > $STARTTIMEFILE
STARTTIME=$(cat $STARTTIMEFILE)

UUIDRUN=$(uuidgen)
echo $UUIDRUN > $RUN_UUID

COMMITDESC="Bench run: $STARTTIME $kUUIDRUN"
git add .
git commit -m "$COMMITDESC"
git push

#openstack compute service list -f value | grep nova-compute | awk '/up/{print $3}' | sort > /home/ubuntu/git/fio-public/hypervisors
HYPERVISORS=$(cat $BASE_DIR/hypervisors | xargs )

for nvm in $(seq 1 $VMSHYPERVISOR)
do
  for host in $HYPERVISORS
  do
    SERVERID=$(openstack server create -f value -c id --network $NETWORK --image $IMAGE --flavor $FLAVOR --boot-from-volume $VOLUMESIZE --host $host --os-compute-api-version 2.74 $UUIDRUN-$host-$nvm) #--security-group $SECGROUP
    # workaround, ipv6 networking not working
    FLOATINGIP=$(openstack floating ip list -f value | grep None | awk '{print $2}' | head -1)
    openstack server add floating ip $SERVERID $FLOATINGIP
    openstack server add security group $SERVERID $SECGROUP
    echo $SERVERID $FLOATINGIP
  done
done

echo "Benchmark vms are being created and the stress test will start at $STARTTIME"
